package mayerbeats.com.ownrssreader;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Mayer on 2/8/2018.
 */

public class RssElement {

    public String title;
    public String urlImage;
    public String urlSource;
    public Date datePubl;

    public RssElement(String title, String urlImage, String urlSource, Date date) {
        this.title = title;
        this.urlImage = urlImage;
        this.urlSource = urlSource;
        this.datePubl = date;
    }

    public String getTitle() {
        return this.title;
    }

    public Date getDatePubl() {
        return this.datePubl;
    }

}
