package mayerbeats.com.ownrssreader;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Button seachButton;
    private RecyclerView rssRecycler;
    private List<RssElement> rssElementList = new ArrayList<>();
    private List<RssElement> tempList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rssRecycler = (RecyclerView) findViewById(R.id.rssRecycler);
        rssRecycler.setLayoutManager(new LinearLayoutManager(this));
        seachButton = (Button) findViewById(R.id.button);
        seachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Action when user clicks searchButton (Search)
                Toast.makeText(MainActivity.this,
                        "Start RSS parsing.",
                        Toast.LENGTH_LONG).show();
                new FetchFeed().execute((Void) null);
            }
        });
    }

    private class FetchFeed extends AsyncTask<Void, Void, Boolean> {
        // https://lifehacker.com/rss
        // http://feeds.feedburner.com/TechCrunch/
        private String[] urlParsing = { "http://feeds.feedburner.com/TechCrunch/",
                                        "https://lifehacker.com/rss" };

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                for (int i = 0; i < urlParsing.length; i++) {
                    tempList.clear();
                    URL url = new URL(urlParsing[i]);
                    InputStream inputStream = url.openConnection().getInputStream();
                    tempList = parseFeed(inputStream);
                    for (int j = 0; j < tempList.size(); j++) {
                        // RssElement current = new RssElement(tempList.get(j).title, tempList.get(j).urlImage, tempList.get(j).urlSource, (tempList.get(j).datePubl).toString());
                        RssElement current = tempList.get(j);
                        rssElementList.add(current);
                    }
                    Log.d("MyXmlParser", "For CONTINUE");
                }
                Log.d("MyXmlParser", "rssElementList size is " + rssElementList.size());
                for (int m = 0; m < rssElementList.size(); m++) {
                    Log.d("MyXmlParser", "rssElementList loop TITLE: " + rssElementList.get(m).title);
                    Log.d("MyXmlParser", "rssElementList loop urlImage: " + rssElementList.get(m).urlImage);
                    Log.d("MyXmlParser", "rssElementList loop urlSource: " + rssElementList.get(m).urlSource);
                    Log.d("MyXmlParser", "rssElementList loop datePubl: " + rssElementList.get(m).datePubl);
                }
                Collections.sort(rssElementList, new Comparator<RssElement>() {
                    @Override
                    public int compare(RssElement rssElement, RssElement t1) {
                        return rssElement.datePubl.compareTo(t1.datePubl);
                    }
                });
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Error ", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error ", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                Toast.makeText(MainActivity.this,
                        "Success.",
                        Toast.LENGTH_LONG).show();
                rssRecycler.setAdapter(new RssElementAdapter(rssElementList));
            } else {
                Toast.makeText(MainActivity.this,
                        "Failed.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    public List<RssElement> parseFeed(InputStream inputStream) throws IOException,
            XmlPullParserException {
        String title = null;
        String urlImage = null;
        String urlSource = null;
        Date datePub = null;
        boolean getThumbnail = true;
        boolean isItem = false;
        List<RssElement> items = new ArrayList<>();

        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);
            xmlPullParser.nextTag();

            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                int eventType = xmlPullParser.getEventType();
                String nameTag = xmlPullParser.getName();
                if (nameTag == null) {
                    continue;
                }
                if (eventType == xmlPullParser.END_TAG) {
                    if (nameTag.equalsIgnoreCase("item")) {
                        if (getThumbnail) {
                            urlImage = "None";
                            if (title != null && urlSource != null && urlImage != null && datePub != null) {
                                if (isItem) {
                                    RssElement item = new RssElement(title, urlImage, urlSource, datePub);
                                    Log.d("MyXmlParser", "Adding item ==>" + title);
                                    Log.d("MyXmlParser", "Adding item ==>" + urlImage);
                                    Log.d("MyXmlParser", "Adding item ==>" + urlSource);
                                    Log.d("MyXmlParser", "Adding item ==>" + datePub);
                                    items.add(item);
                                    title = null;
                                    urlImage = null;
                                    urlSource = null;
                                    datePub = null;
                                    isItem = false;
                                }
                            }
                        }
                        isItem = false;
                        Log.d("MyXmlParser", "IS ITEM END TAG" + isItem);
                    }
                    continue;
                }
                if (eventType == xmlPullParser.START_TAG) {
                    if (nameTag.equalsIgnoreCase("item")) {
                        isItem = true;
                        getThumbnail = true;
                        Log.d("MyXmlParser", "IS ITEM START TAG" + isItem);
                        continue;
                    }
                }
                String result = "";
                Log.d("MyXmlParser", "Parsing name ==> " + nameTag);
                if(xmlPullParser.next() == xmlPullParser.TEXT) {
                        result = xmlPullParser.getText();
                        xmlPullParser.nextTag();
                }
                if (nameTag.equalsIgnoreCase("title")) {
                    title = result;
                    Log.d("MyXmlParser", "Title now ==> " + title);
                } else if (nameTag.equalsIgnoreCase("media:thumbnail")) {
                    if (getThumbnail) {
                        urlImage = xmlPullParser.getAttributeValue(null, "url");
                        // urlImage = "https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
                        Log.d("MyXmlParser", "Parsing URL IMAGE ==> " + xmlPullParser.getAttributeValue(null, "url"));
                        getThumbnail = false;
                    }
                } else if (nameTag.equalsIgnoreCase("link")) {
                    urlSource = result;
                    Log.d("MyXmlParser", "Link now ==> " + urlSource);
                } else if (nameTag.equalsIgnoreCase("pubDate")) {
                    Log.e("MyXmlParsed", "pubDate now before ==> " + result);
                    DateFormat df = new SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
                    try {
                        datePub = df.parse(result.replaceAll(",",""));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (title != null && urlSource != null && urlImage != null ) {
                    if (isItem) {
                        RssElement item = new RssElement(title, urlImage, urlSource, datePub);
                        Log.d("MyXmlParser", "Adding item ==>" + title);
                        Log.d("MyXmlParser", "Adding item ==>" + urlImage);
                        Log.d("MyXmlParser", "Adding item ==>" + urlSource);
                        Log.d("MyXmlParser", "Adding item ==>" + datePub);
                        items.add(item);
                        title = null;
                        urlImage = null;
                        urlSource = null;
                        datePub = null;
                        isItem = false;
                    }

                }
            }
            return items;
        } finally {
            inputStream.close();
        }
    }

    public class RssElementAdapter
        extends RecyclerView.Adapter<RssElementAdapter.FeedModelViewHolder> {

        private List<RssElement> rssElementList;

        public class FeedModelViewHolder extends RecyclerView.ViewHolder {
            private View rssFeedView;

            public FeedModelViewHolder(View v) {
                super(v);
                rssFeedView = v;
            }
        }

        public RssElementAdapter(List<RssElement> rssElementList) {
            this.rssElementList = rssElementList;
        }

        @Override
        public FeedModelViewHolder onCreateViewHolder(ViewGroup parent, int type) {
            View v = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_rss_element, parent, false);
            FeedModelViewHolder holder = new FeedModelViewHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(FeedModelViewHolder holder, int position) {
            final RssElement rssElement = rssElementList.get(position);
            ((TextView)holder.rssFeedView.findViewById(R.id.tittleText)).setText(rssElement.title);

            //((ImageView)holder.rssFeedView.findViewById(R.id.imageElement)).setImageBitmap(mIcon11);
            if (rssElement.urlImage != "None") {
                Log.d("MyXMLParser", "URL IMAGE ISNOT NONE");
                new FetchImage(((ImageView)holder.rssFeedView.findViewById(R.id.imageElement))).execute(rssElement.urlImage);
            } else {
                ((ImageView)holder.rssFeedView.findViewById(R.id.imageElement)).setImageResource(android.R.color.transparent);
            }

            ((TextView)holder.rssFeedView.findViewById(R.id.urlSource)).setText(rssElement.urlSource);

            ((LinearLayout)holder.rssFeedView.findViewById(R.id.linearLayout)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(rssElement.urlSource);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
        }

        public class FetchImage extends AsyncTask<String, Void, Bitmap> {

            private ImageView imageView;

            public FetchImage(ImageView imageView) {
                this.imageView = imageView;
            }

            @Override
            protected Bitmap doInBackground(String... string) {
                String url = string[0];
                Bitmap bmp = null;
                try {
                    InputStream in = new java.net.URL(string[0]).openStream();
                    bmp = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e(TAG, "Error ", e);
                    e.printStackTrace();
                }
                return bmp;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                imageView.setImageBitmap(result);
            }
        }

        @Override
        public int getItemCount() {
            return rssElementList.size();
        }
    }

}
